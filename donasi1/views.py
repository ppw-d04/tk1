from django.shortcuts import render,redirect
from .forms import DonasiTunaiForm
from .models import DonasiTunai

# Create your views here.
def donasi_tunai(request):
    if request.method == "POST":
        form = DonasiTunaiForm(request.POST or None)
        if form.is_valid:
            form.save()
        return redirect('/donasi_tunai/list_donasitunai')
    else:
        form = DonasiTunaiForm()
        response = {'form' : form}
    return render(request, 'form_donasitunai.html', response)

def list_donasitunai(request):
    donatur = DonasiTunai.objects.all()
    response = {'donatur':donatur}
    return render(request, 'list_donasitunai.html', response)
