from django.urls import path
from . import views

# Create your views here.
app_name = 'donasi1'

urlpatterns = [
    path('', views.donasi_tunai, name='donasi_tunai'),
	path('list_donasitunai/', views.list_donasitunai, name='list_donasitunai')
]

