from django.db import models

# Create your models here.
class DonasiTunai(models.Model):
	nama = models.CharField('Nama', max_length=120)
	alamat = models.TextField('Alamat')
	kontak = models.CharField('Email atau No. Handphone', max_length=100)
	jumlah = models.CharField('Jumlah Transfer', max_length=50)
	anon = models.BooleanField('Tampilkan sebagai donatur anonim', default=False)