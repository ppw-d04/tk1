from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import DonasiNonTunai
from .forms import DonasiNonTunaiForm
# Create your views here.

def donasi_nontunai(request):
    submitted = False
    if request.method == 'POST' :
        form = DonasiNonTunaiForm(request.POST)
        form.save()
        return HttpResponseRedirect('/donasi_nontunai/?submitted=True')
    else :
        form = DonasiNonTunaiForm()
        if 'submitted' in request.GET :
            submitted = True
        context = {'form' : form, 'submitted' : submitted}  

    return render(request, 'form_donasinontunai.html', context)    

def list_donasinontunai(request):
    donatur = DonasiNonTunai.objects.all()
    context = {'donatur' : donatur}
    return render(request, 'list_donasinontunai.html', context)     

