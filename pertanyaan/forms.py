from django import forms
from .models import Pertanyaan

class Form (forms.ModelForm):
	class Meta:
		model = Pertanyaan
		fields = '__all__'

		error_messages={
			'required':'Please type'
		}