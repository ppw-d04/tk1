from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .forms import Form
from .models import Pertanyaan

def asked_us(request):
	submitted = False
	if request.method == 'POST':
		form = Form(request.POST)
		if form.is_valid():
			form.save()
			return HttpResponseRedirect('/asked_us/?submitted=True')
	else:
		form = Form()
		if 'submitted' in request.GET:
			submitted = True
		context = {
			'form' : form,
			'submitted' : submitted
		}

	return render(request, 'form_pertanyaan.html', context)

def list_asked(request):
	mylist = Pertanyaan.objects.all()
	return render(request, 'list_result.html', {'mylist':mylist})