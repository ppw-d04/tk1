from django.contrib import admin
from .models import Pertanyaan

# Register your models here.
@admin.register(Pertanyaan)
class ActivityAdmin(admin.ModelAdmin):
	list_display = ('nama','kontak','pertanyaan')
