from django.test import TestCase, Client
from .views import asked_us, list_asked
from .models import Pertanyaan

class TestPertanyaan(TestCase):
	def test_apakah_url_asked_us_ada(self):
		response = Client().get('/asked_us/')
		self.assertEquals(response.status_code, 200)

	def test_apakah_asked_us_ada_htmlnya(self):
		response = Client().get('/asked_us/')
		self.assertTemplateUsed(response, 'form_pertanyaan.html')

	def test_apakah_models_add_activity_ada(self):
		Pertanyaan.objects.create(nama="abc", kontak="example@gmail.com", pertanyaan='Siapa penemu bumi?')
		self.assertEqual(Pertanyaan.objects.all().count(), 1)

class TestList(TestCase):
	def test_apakah_url_list_asked_ada(self):
		response = Client().get('/list_asked/')
		self.assertEquals(response.status_code, 200)
	def test_apakah_list_asked_ada_htmlnya(self):
		response = Client().get('/list_asked/')
		self.assertTemplateUsed(response, 'list_result.html')